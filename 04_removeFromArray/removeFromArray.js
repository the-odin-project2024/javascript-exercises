const removeFromArray = function(arr, ...value) {

    arr = arr.filter(arg => !value.includes(arg))
    return arr
}

// Do not edit below this line
module.exports = removeFromArray;
