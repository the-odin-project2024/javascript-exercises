const sumAll = function(num1, num2) {
    // Error Handling
    if((num1 < 0 || typeof(num1) != "number") 
        || (num2 < 0 || typeof(num2) != "number") ) {
        return "ERROR"
    }
    
    // Switches the numbers around if the first one is bigger than the second one
    if (num1 > num2) {
        temp = num2;
        num2 = num1;
        num1 = temp
    }
    let num4 = num2;
    while (num2 >= num1) {
        let num3 = num2 -1
        num4 = num4 + num3
        num2--;
    }
    return num4
};

// Do not edit below this line
module.exports = sumAll;
