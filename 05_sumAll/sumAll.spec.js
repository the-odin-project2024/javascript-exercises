const sumAll = require("./sumAll")

describe("sumAll", () => {
  test("Sums numbers within the range", () => {
    expect(sumAll(1, 4)).toEqual(10);
  });
  test("Works with large numbers", () => {
    expect(sumAll(1, 4000)).toEqual(8002000);
  });
  test("Works with larger number first", () => {
    expect(sumAll(123, 1)).toEqual(7626);
  });
  test("Returns ERROR with negative numbers", () => {
    expect(sumAll(-10, 4)).toEqual("ERROR");
  });
  test("Return ERROR with non-number parameter", () => {
    expect(sumAll(10, "90")).toEqual("ERROR");
  });
  test("Returns ERROR with non-number parameters", () => {
    expect(sumAll(10, [90, 1])).toEqual("ERROR")
  })
})