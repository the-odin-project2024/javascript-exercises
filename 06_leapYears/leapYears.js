const leapYears = function(leapYear) {
    /*
    Leap Years:
        Divisible by FOUR IF the year is divisible by 100 it is not a leap year UNLESS it is divisible by 400
    */
   if (leapYear % 4 == 0) {
    if (leapYear % 400 == 0 && leapYear % 100 == 0) {
        return true;
    }
    if (leapYear % 100 == 0) {
        return false;
    }
    return true;
   } else {
    return false;
   }
}
// Do not edit below this line
module.exports = leapYears;
