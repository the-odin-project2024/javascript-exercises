const leapYears = require("./leapYears")

describe("leapYears", () => {
  test("Works with non century years", () => {
    expect(leapYears(1996)).toBe(true);
  });
  test("Works with non century years", () => {
    expect(leapYears(1997)).toBe(false);
  });
  test("Works with ridiculously futuristic non century years", () => {
    expect(leapYears(34992)).toBe(true);
  });
  test("Works with century years", () => {
    expect(leapYears(1900)).toBe(false);
  });
  test("Works with century years", () => {
    expect(leapYears(1600)).toBe(true);
  });
  test("Works with century years", () => {
    expect(leapYears(700)).toBe(false);
  });
})