const repeatString = function(str, num) {
    let repeatWord = ""
    while (num > 0) {
        repeatWord += str;
        num--;
    }
    return repeatWord;
};

// Do not edit below this line
module.exports = repeatString;
