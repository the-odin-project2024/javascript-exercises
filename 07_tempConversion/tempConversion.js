const convertToCelsius = function(num) {
  const celsius = (num - 32) / (9/5);
  return Math.round(celsius * 10) / 10
};

const convertToFahrenheit = function(num) {
  const fahrenheit = (num * 1.8) + 32;
  return Math.round(fahrenheit * 10) / 10
};

// Do not edit below this line
module.exports = {
  convertToCelsius,
  convertToFahrenheit
};
